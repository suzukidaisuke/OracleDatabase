# Oracle database の勉強方法
* **何よりも過去問!!!**  
資格試験対策の意味でも、エッセンスがまとまってるという意味でも  
…とはいえ、ポイントは幾つかあるのでそれを少し説明 （間違ってる部分があったらゴメンナサイ）

* 鈴木がよく見てた（気がする）サイト  
 * http://oracle.se-free.com/  ここの上3つ（SQL）
 * http://luna.gonna.jp/oracle/ora_select.html


# join, where, group by, order byの順番
これらを全部使った場合、次の優先順位でsqlが実行される  
1. join --> テーブル結合して新たなテーブルを作る  
2. where --> 新たなテーブルから必要なものだけ選択  
3. group by --> 選択したデータを集計  
4. having --> 集計結果から、ここで指定した内容に該当するものを抽出  
5. order by --> 結果を並び替え  

参考: http://mitene.or.jp/~rnk/TIPS_ORCL_SELECT2.htm


# joinのイメージ
テーブルを横に合体させる（列を増やす）  
参考: http://mitene.or.jp/~rnk/TIPS_ORCL_TBLLINK1.htm

table_A

|No|名前|
|:--:|:--:|
|1|suzuki|
|2|fujita|
|3|tanahashi|
|4|ogata|

table_B

|No|性別|
|:--:|:--:|
|1|男|
|2|女|
|3|男|

table_A と table_B の **inner join(結合キー:"No")**

|No|名前|性別|
|:--:|:--:|:--:|
|1|suzuki|男|
|2|fujita|女|
|3|tanahashi|男|

* 補足（Oracleのテスト対策程度）
↓こういう (+) 記号を使った結合もあってテストに出ることもあるので、「こんなのもあるんだ～」  
程度に覚えていて損は無いかも。（自分もその程度の認識）  
http://qiita.com/naotarou/items/aa78ae725d128b6e5a12


# unionのイメージ
テーブルを下に合体させる（行を増やす）  
参考:http://www.shift-the-oracle.com/sql/union-operator.html

table_A

|No|名前|
|:--:|:--:|
|1|suzuki|
|2|fujita|

table_B

|No|名前|
|:--:|:--:|
|3|tanahashi|
|4|ogata|

table_A と table_B の **union**

|No|名前|
|:--:|:--:|
|1|suzuki|
|2|fujita|
|3|tanahashi|
|4|ogata|


# where, group by, order by の条件部で気を付けること
例えば "select __count(性別) AS 合計__ ..." のように項目に別名をつけていて、この列を  
キーに where, group by, order by したい場合  

* where --> count(性別) を使わなきゃダメ (where 合計 > 10 等はNG)
* group by --> count(性別) を使わなきゃダメ (group by 合計 はNG)
* order by --> count(性別) でも別名(合計)でもOK (order by 合計 もOK)

ざっくりとした理由 --> http://h-h-h-h-h-h.blogspot.jp/2009/12/group-byhavingselect.html  
具体例(こんな感じで超長くなることも>_<) --> http://blog.livedoor.jp/akf0/archives/51064573.html


# 文字型の暗黙変換
暗黙変換とは、その名の通りOracleが自動的にあるデータ型から別のデータ型への変換を行うこと。  
例えば、__'1'(文字列) + 1(数値)__ みたいなのが計算されてしまう。  
便利なように感じるかもしれないが、このせいでバグになってしまう事も多いので、明示的にデータ型変換をしたほうがいい。  

参考 : http://www.atmarkit.co.jp/ait/articles/0508/16/news050.html  
(「Oracle 変換 暗黙」等でweb検索すると色々出てくるので、そちらを見てもいいかも)


# 制約の設定方法2パターン
参考 : http://luna.gonna.jp/oracle/ora_const.html  
* 列制約 : カラム毎に制約を記載する
```
CREATE TABLE SHAIN_MASTER(
  ID          VARCHAR2(10)　PRIMARY KEY,
  NAME        VARCHAR2(10)　NOT NULL,
  TEL         VARCHAR2(10)　UNIQUE,
  AGE         NUMBER(2)　CHECK(AGE BETWEEN 18 AND 65),
  BUSHO_CD    CHAR(2)　REFERENCES BUSHO_MASTER(BUSHO_CD)
);
```

* 表制約 : 全カラム定義した後に、まとめて制約を記載する
```
CREATE TABLE SHAIN_MASTER(
  ID1         VARCHAR2(5),
  ID2         VARCHAR2(10),
  NAME        VARCHAR2(10),
  TEL         VARCHAR2(10),
  AGE         NUMBER(2),
  BUSHO_CD    CHAR(2),
  CONSTRAINT  cons_p1 PRIMAEY KEY(ID1, ID2),
  CONSTRAINT  cons_u1 UNIQUE(TEL),
  CONSTRAINT  cons_c1 CHECK(AGE BETWEEN 18 AND 65),
  CONSTRAINT  cons_f1 FOREIGN KEY(BUSHO_CD)
                          REFERENCES　BUSHO_MASTER(BUSHO_CD)
);
```

* 注意事項
  * 複数の列を指定する制約は、__表制約__でないと定義できない。  
  例) 主キーを「1234 鈴木大輔」のように、id と name の組み合わせで指定したい場合  
  * NOT NULL制約は__列制約__でないと定義できない。


# CASE式 と DECODE関数
http://oracle.se-free.com/dml/04_if.html  
* SQLの中でＩＦ文のような条件式を使いたい場合に使用する  
* CASE式とDECODE関数の違い
 * CASE式では、WHEN句でEXISTSや比較条件やINやBETWEENやLIKEなどの演算子が使用可能
 * DECODE関数では "NULL = NULL" の評価は True となるが、CASE式ではNULLとなるCASE式で  
  NULLを評価する場合は、WHEN句でIS NULLを使う。(???)
 * ELSE句（デフォルト値）は省略可能（省略されたときはNULL値が戻される）  
  しかし、バグの原因になることがあるので、ELSE句を記述する癖をつけておいた方が良いらしい。


# 【これから書く予定】
## 時刻に変換する際のfm とかfxみたいなの
## 副問い合わせで混乱しないために（
分割して考えると良い的な話

## in, all, any
## not in, exist
